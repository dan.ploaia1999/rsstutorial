package com.journaldev.androidrssfeedtutorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    ArrayList<String> rssLinks = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnEYEnew = findViewById(R.id.btnEYEnew);
        Button btnCinemaBlend = findViewById(R.id.btnCinemaBlend);
        Button btnNyTimes = findViewById(R.id.btnNyTimes);
        btnEYEnew.setOnClickListener(this);
        btnCinemaBlend.setOnClickListener(this);
        btnNyTimes.setOnClickListener(this);
        rssLinks.add("https://abc7.com/feed/");
        rssLinks.add("https://ktla.com/feed/");
        rssLinks.add("https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnEYEnew:
                startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", rssLinks.get(0)));
                break;

            case R.id.btnCinemaBlend:
                startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", rssLinks.get(1)));
                break;

            case R.id.btnNyTimes:
                startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", rssLinks.get(1)));
                break;
            default:
        }
    }
}
