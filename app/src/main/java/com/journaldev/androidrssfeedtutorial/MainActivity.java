package com.journaldev.androidrssfeedtutorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    ArrayList<String> rssLinks = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnRediff = findViewById(R.id.btnjurnal);
        Button btnCinemaBlend = findViewById(R.id.btnSport);
        btnRediff.setOnClickListener(this);
        btnCinemaBlend.setOnClickListener(this);

        boolean add = rssLinks.add("https://jurnaltv.md/rss/ro/all");
        rssLinks.add("https://www.sport.ro/stiri/rss");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnjurnal:
                startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", rssLinks.get(0)));
                break;

            case R.id.btnSport:
                startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", rssLinks.get(1)));
                break;
        }
        
    }
}
